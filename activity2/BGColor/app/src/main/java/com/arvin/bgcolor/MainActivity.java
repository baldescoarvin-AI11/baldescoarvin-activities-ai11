package com.arvin.bgcolor;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    View screenView;
    Button changeColor;
    int[] BGColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BGColor = new int[] {Color.BLUE, Color.RED, Color.CYAN, Color.GREEN};

        screenView = findViewById(R.id.rView);
        changeColor = (Button) findViewById(R.id.button);

        changeColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int aLength = BGColor.length;

                Random random = new Random();
                int rNo = random.nextInt(aLength);

                screenView.setBackgroundColor(BGColor[rNo]);
            }
        });
    }
}